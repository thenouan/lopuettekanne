import os.path

def kuva_failid(kaust):
    for nimi in os.listdir(kaust):
        täisnimi = os.path.join(kaust, nimi)

        # Täienda koodi, et prinditaks välja ka alamkaustade sisu.
        # Abiks tuleb os.path.isdir() funktsioon.
        print("Fail", täisnimi)

kuva_failid("/")
