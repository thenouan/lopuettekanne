
def fibo(n):
    ...

print(fibo(0) == 0)
print(fibo(1) == 1)
print(fibo(2) == 1)
print(fibo(3) == 2)
print(fibo(4) == 3)
print(fibo(5) == 5)
