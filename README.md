# Hariduslikud programmeerimiskeeled

# Rekursioon pythoni näidetel


## Sissejuhatus

Paljude ülesannete lahendamisel on suureks abiks, kui ülesanne jaotada alamülesanneteks
ning kasutada korduvate osade juures tsükkleid.

Ilmneb, et taoliste ülesannete puhul on tsüklite asemel võimalik kasutada ka rekursiivseid funktsioone – s.o 
funktsioone, mis iseennast välja kutsuvad. Tihti on selliselt kirjutatud lahendused selgemad ja lühemad kui 
tsüklitega kirjutatud lahendused.


## Rekursioonist lähemalt

Rekursiooniks nimetatakse defineeritava funktsiooni väljakutset selle sama funktsiooni sees.

```python
def f():
	f()
	...
```





### Näide: failipuu printimine

Proovime kirjutada funktsiooni, mis prindib ekraanile etteantud failisüsteemi asukohas leiduvad failid koos
seal sisalduvate alamkaustadega.

Baaskood asub `./list_dir.py`

Täiendada koodi, et prinditaks välja ka alamkaustade sisu.


### Ülesanne: Fibonacci arvud

Kirjutada funktsioon, mis prindib välja Fibonacci arvu kohal `n`.

Baaskood asub `./fibonacci.py`

### Näide: Sierpinski kolmnurk

Rekursiooni on väga lihtne kasutada fraktalite genereerimiseks. Fraktalid on abstraktsed kujundid,
mis sisaldavad endas sama mustrit läbi terve kujundi. Parema ettekujutuse saamiseks, klõpsa alloleval pildil.

[![Mandelbrot set](https://img.youtube.com/vi/0jGaio87u3A/0.jpg)](https://www.youtube.com/watch?v=0jGaio87u3A&t=15s)


Vaatame ühte lihtsamat fraktalit, mille kood asub `./sierpinski.py`


### Ülesanne: vabaltvalitud fraktal.

Tutvume [Lindenmayer süsteemiga](https://en.wikipedia.org/wiki/L-system). See on viis, mis võimaldab
fraktalite genereerimise algoritme kergelt kirja panna. Alguses võtab natukene aega grammatikaga
tutvumiseks, aga kui asi käpas, siis on üsna kerge neid algoritme Pythonis kirja panna.

