import turtle as t

# Et muuta turtle liikumine kiiremaks.
t.speed(0)
t.delay(0)
t.tracer(8, 25)


def sierpinski(size, depth):
    if depth == 0: return
    for i in range(3):
        t.forward(size / 2)
        t.left(120)
        sierpinski(size / 2, depth - 1)
        t.right(120)
        t.forward(size / 2)
        t.left(120)


sierpinski(128, 5)

t.exitonclick()

